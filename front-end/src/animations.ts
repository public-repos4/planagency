import {
    animate,
    animateChild,
    group,
    query,
    stagger,
    style,
    state,
    transition,
    trigger
} from "@angular/animations";

export const listAnimation = trigger("listAnimation", [
    transition("* => *", [
        // each time the binding value changes
        query(
            ":enter",
            [
                style({ opacity: 0 }),
                stagger(500, [animate("0.5s", style({ opacity: 1 }))])
            ],
            { optional: true }
        )
    ])
]);
