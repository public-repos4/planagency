import {ModuleWithProviders, NgModule} from '@angular/core';
import {NavbarComponent} from "./app/components/layouts/navbar/navbar.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [
        NavbarComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
    ],
    exports: [
        CommonModule,

        NavbarComponent,
    ],
})

export class SharedModule {

}
