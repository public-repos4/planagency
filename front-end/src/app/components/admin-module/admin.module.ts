import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared.module";
import {NavbarComponent} from "../layouts/navbar/navbar.component";
import {CommonModule} from "@angular/common";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DataTablesModule} from "angular-datatables";
import {NgSelectModule} from "@ng-select/ng-select";
import {CategoriesListComponent} from "./pages/categories/categories-list.component";
import {AddEditCategoryComponent} from "./pages/categories/dialogs/add-edit-category-dialog/add-edit-category-dialog.component";
import {DeleteCategoryConfirmationDialogComponent} from "./pages/categories/dialogs/delete-category-confirmation-dialog/delete-category-confirmation-dialog.component";
import {ProductsListComponent} from "./pages/products/products-list.component";
import {DeleteProductConfirmationDialogComponent} from "./pages/products/dialogs/delete-product-confirmation-dialog/delete-product-confirmation-dialog.component";
import {AddEditProductComponent} from "./pages/products/dialogs/add-edit-product-dialog/add-edit-product-dialog.component";
import {PageNotFoundComponent} from "../public-module/pages/page-not-found/page-not-found.component";

const routes: Routes = [
    {
        path: 'categories', component: CategoriesListComponent,
        children: [
            {path: 'add', component: AddEditCategoryComponent},
            {path: 'edit/:id', component: AddEditCategoryComponent},
            {path: 'delete/:id', component: DeleteCategoryConfirmationDialogComponent}
        ]
    },
    {
        path: 'products', component: ProductsListComponent,
        children: [
            {path: 'add', component: AddEditProductComponent},
            {path: 'edit/:id', component: AddEditProductComponent},
            {path: 'delete/:id', component: DeleteProductConfirmationDialogComponent}
        ]
    },
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    declarations: [
        CategoriesListComponent,
        AddEditCategoryComponent,
        DeleteCategoryConfirmationDialogComponent,

        ProductsListComponent,
        AddEditProductComponent,
        DeleteProductConfirmationDialogComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,

        CommonModule,
        FormsModule,
        NgbModule,
        DataTablesModule,
        NgSelectModule,
        SharedModule,

    ],
    exports: [
        NavbarComponent,
    ],
    providers: [],
})
export class AdminModule {

}
