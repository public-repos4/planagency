import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../../../../_services/alert.service';
import {CategoriesService} from '../../../../../../_services/categories.service';
import {SelectOption} from '../../../../../../_models/select-option';
import {Product} from '../../../../../../_models/product/product';
import {ProductsService} from '../../../../../../_services/products.service';
import {ProductSubmit} from '../../../../../../_models/product/product-submit';
import {dietaryFlagsPickList} from '../../../../../../_models/enums';
import {regexes} from '../../../../../../_models/regexes';
import {EnumsService} from "../../../../../../_services/enums.service";

@Component({
    selector: 'add-edit-product-dialog',
    templateUrl: './add-edit-product-dialog.component.html',
})

export class AddEditProductComponent implements OnInit, OnDestroy {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private productsService: ProductsService,
        private categoriesService: CategoriesService,
        private enumsService: EnumsService,
    ) {
    }

    @ViewChild('addEditModal', {static: true}) addEditModal: TemplateRef<any>;
    destroy = new Subject<any>();

    form: FormGroup;
    id: number;
    isAddMode: boolean;
    loading = false;
    submitted = false;

    product: Product;
    productSubmit: ProductSubmit;
    categoriesPickList: Observable<SelectOption[]>;
    dietaryFlagsPickList: Observable<SelectOption[]>;
    image: File = null;

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            id: [],
            name: ['', Validators.required],
            categoryId: ['', Validators.required],
            image: ['', Validators.required],
            price: ['', [Validators.required, Validators.pattern(regexes.float), Validators.min(0), Validators.max(9999)]],
            dietaryFlagId: ['', Validators.required],
            numOfViews: ['', [Validators.required, Validators.pattern(regexes.int), Validators.min(0), Validators.max(10000)]],
            description: ['', Validators.required],
        });

        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.categoriesPickList = this.categoriesService.getSelectOptions();
        this.categoriesPickList.subscribe((items) => {
            if (!this.isAddMode) {
                this.form.setValue({
                    ...this.form.value,
                    categoryId: this.product?.category?.id || '',
                });
            }
        });

        this.dietaryFlagsPickList = this.enumsService.getSelectOptions('Dietary_Flags');
        this.dietaryFlagsPickList.subscribe((items) => {
            if (!this.isAddMode) {
                this.form.setValue({
                    ...this.form.value,
                    dietaryFlagId: this.product?.dietaryFlag?.id || '',
                });
            }
        });

        if (!this.isAddMode) {
            this.productsService.getById(this.id)
                .subscribe(product => {
                    this.product = product;
                    this.form.setValue({
                        id: product.id,
                        name: product.name,
                        categoryId: product?.category?.id || '',
                        price: product.price,
                        image: '',
                        dietaryFlagId: product.dietaryFlag.id,
                        numOfViews: product.numOfViews,
                        description: product.description,
                    });
                });
        }

        this.modalService.open(this.addEditModal, {
            // size: 'sm',
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/products');
        }, (reason) => {
            this.router.navigateByUrl('/admin/products');
            // console.log('reason')
        });
    }

    ngOnDestroy() {
        this.destroy.next();
    }

    // convenience getter for easy access to form fields
    get productForm() {
        return this.form.controls;
    }

    onFileChange($event) {
        if ($event.target.files.length <= 0) return;
        this.image = $event.target.files[0];
    }

    onSubmit() {
        console.log(this.form.value)
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.productSubmit = {
            name: this.form.get('name').value,
            categoryId: this.form.get('categoryId').value,
            image: this.image,
            price: this.form.get('price').value,
            dietaryFlagId: this.form.get('dietaryFlagId').value,
            numOfViews: this.form.get('numOfViews').value,
            description: this.form.get('description').value,
        };

        this.loading = true;

        if (this.isAddMode) {
            this.addCategory();
        } else {
            this.editCategory();
        }
    }

    private addCategory() {
        this.productsService.add(this.productSubmit)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededAdd', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    private editCategory() {
        this.productSubmit.id = this.form.get('id').value;

        this.productsService.edit(this.id, this.productSubmit)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededEdit', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}
