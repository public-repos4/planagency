﻿import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CategoriesService} from "../../../../../../_services/categories.service";
import {AlertService} from "../../../../../../_services/alert.service";
import {first} from "rxjs/operators";
import {ProductsService} from "../../../../../../_services/products.service";

@Component({
    selector: 'delete-product-confirmation-dialog',
    templateUrl: './delete-product-confirmation-dialog.component.html',
})
export class DeleteProductConfirmationDialogComponent implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private productsService: ProductsService,
    ) {
    }

    @ViewChild('deleteConfirmationModal', {static: true}) deleteConfirmationModal: TemplateRef<any>;

    id: number;
    loading = false;
    submitted = false;

    public ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];

        this.modalService.open(this.deleteConfirmationModal, {
            size: 'sm',
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/products');
        }, (reason) => {
            this.router.navigateByUrl('/admin/products');
            // console.log('reason')
        });
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        this.loading = true;
            this.deleteProduct();
    }

    private deleteProduct() {
        this.productsService.delete(this.id)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededDelete', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

}
