import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "../../../../../../_services/alert.service";
import {CategoriesService} from "../../../../../../_services/categories.service";
import {SelectOption} from "../../../../../../_models/select-option";
import {Category} from "../../../../../../_models/category/category";

@Component({
    selector: 'add-edit-category-dialog',
    templateUrl: './add-edit-category-dialog.component.html',
})

export class AddEditCategoryComponent implements OnInit, OnDestroy {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private categoriesService: CategoriesService,
    ) {
    }

    @ViewChild('addEditModal', {static: true}) addEditModal: TemplateRef<any>;
    destroy = new Subject<any>();

    form: FormGroup;
    id: number;
    isAddMode: boolean;
    loading = false;
    submitted = false;

    category: Category;
    categoriesPickList: Observable<SelectOption[]>;

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            id: [],
            name: ['', Validators.required],
            parentCategoryId: [''],
        });

        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.categoriesPickList = this.categoriesService.getSelectOptions();
        this.categoriesPickList.subscribe((items) => {
            this.form.setValue({
                ...this.form.value,
                parentCategoryId: this.category?.parentCategory?.id || '',
            });
        });

        if (!this.isAddMode) {
            this.categoriesService.getById(this.id)
                .subscribe(category => {
                    this.category = category;
                    this.form.setValue({
                        id: category.id,
                        name: category.name,
                        parentCategoryId: category?.parentCategory?.id || '',
                    });
                });
        }

        this.modalService.open(this.addEditModal, {
            size: 'sm',
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/categories');
        }, (reason) => {
            this.router.navigateByUrl('/admin/categories');
            // console.log('reason')
        });
    }

    ngOnDestroy() {
        this.destroy.next();
    }

    // convenience getter for easy access to form fields
    get categoryForm() {
        return this.form.controls;
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        if (this.isAddMode) {
            this.addCategory();
        } else {
            this.editCategory();
        }
    }

    private addCategory() {
        this.categoriesService.add(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededAdd', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    private editCategory() {
        this.categoriesService.edit(this.id, this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededEdit', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}
