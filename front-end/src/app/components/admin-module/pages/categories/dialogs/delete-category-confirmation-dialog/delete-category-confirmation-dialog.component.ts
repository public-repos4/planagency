﻿import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CategoriesService} from "../../../../../../_services/categories.service";
import {AlertService} from "../../../../../../_services/alert.service";
import {first} from "rxjs/operators";

@Component({
    selector: 'delete-category-confirmation-dialog',
    templateUrl: './delete-category-confirmation-dialog.component.html',
})
export class DeleteCategoryConfirmationDialogComponent implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private categoriesService: CategoriesService,
    ) {
    }

    @ViewChild('deleteConfirmationModal', {static: true}) deleteConfirmationModal: TemplateRef<any>;

    id: number;
    loading = false;
    submitted = false;

    public ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];

        this.modalService.open(this.deleteConfirmationModal, {
            size: 'sm',
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/categories');
        }, (reason) => {
            this.router.navigateByUrl('/admin/categories');
            // console.log('reason')
        });
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        this.loading = true;
            this.deleteCategory();
    }

    private deleteCategory() {
        this.categoriesService.delete(this.id)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededDelete', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}
