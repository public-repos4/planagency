import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, Renderer2} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {environment} from "../../../../../environments/environment";
import {DataTableDirective} from 'angular-datatables';

@Component({
    selector: 'categories-list',
    templateUrl: './categories-list.component.html',
})

export class CategoriesListComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor(
        private renderer: Renderer2,
        private router: Router,
    ) {
    }

    @ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        processing: true,

        ajax: {
            headers: {
                'Accept-Language': localStorage.getItem('lang')
            },
            type: 'Post',
            url: `${environment.apiUrl}/categories/getDatatable`,
        },

        rowCallback: (row: Node, data: any[] | Object, index: number) => {
            // Unbind first in order to avoid any duplicate handler
            // (see https://github.com/l-lin/angular-datatables/issues/87)
            // Note: In newer jQuery v3 versions, `unbind` and `bind` are
            // deprecated in favor of `off` and `on`

            console.log(data)
            $(row).find('[data-title="openEditCategoryForm"]').off('click');
            $(row).find('[data-title="openEditCategoryForm"]').on('click', () => {
                this.router.navigateByUrl(`/admin/categories/edit/${data['id']}`);
            });

            $(row).find('[data-title="openDeleteCategoryConfirm"]').off('click');
            $(row).find('[data-title="openDeleteCategoryConfirm"]').on('click', () => {
                this.router.navigateByUrl(`/admin/categories/delete/${data['id']}`);
            });
            return row;
        },
        columnDefs: [
            {
                targets: '_all',
                createdCell: function (td, cellData, rowData, row, col) {
                    let categoriesTblCellsDataTitles = ['id', 'name', 'parentCategory'];
                    $(td).attr('data-title', categoriesTblCellsDataTitles[col]);
                },
                defaultContent: ""
            },
            {orderable: false, targets: [2, 3]}
        ],

        columns: [{
            data: 'id',
            name: 'id'
        }, {
            data: 'name',
            name: 'name'
        }, {
            data: 'parentCategory.name',
            name: 'parentCategory'
        }, {
            data: null,
            name: 'actions',
            render: function (data: any, type: any, full: any) {
                let actions = '';
                actions += `<i class="cursor-pointer icon-compose mr-2" data-title="openEditCategoryForm"></i>`;
                actions += `<i class="cursor-pointer icon-bin mr-2" data-title="openDeleteCategoryConfirm"></i>`;
                return actions;
            }
        }]
    };

    dtTrigger: Subject<any> = new Subject<any>();

    ngOnInit(): void {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd && this.router.url.endsWith('/categories')) {
                this.rerenderDatatable()
            }
        });
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerenderDatatable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();

            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }
}
