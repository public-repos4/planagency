import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../_services/auth.service";

@Component({
    selector: 'app-navbar-two',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(
        private accountService: AuthService
    ) {
    }

    user: any;

    ngOnInit(): void {
        this.user = this.accountService.userValue;
    }

    onLogout() {
        this.accountService.logout();
    }
}
