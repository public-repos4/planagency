import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared.module";
import {NavbarComponent} from "../layouts/navbar/navbar.component";

import {LoginComponent} from "./pages/login/login.component";
import {PageNotFoundComponent} from "./pages/page-not-found/page-not-found.component";
import {PaginationModule} from "ngx-bootstrap/pagination";

const routes: Routes = [
    // {path: '', redirectTo: 'login', pathMatch: 'full'},
    // {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    declarations: [
        LoginComponent,
        PageNotFoundComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,

        FormsModule,
        PaginationModule.forRoot(),

        SharedModule,
    ],
    exports: [
        NavbarComponent,
    ],
    providers: [],
})
export class PublicModule {

}
