import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from 'src/environments/environment';
import {AuthService} from 'src/app/_services/auth.service';

@Injectable()
export class ConfigInterceptor implements HttpInterceptor {
    constructor(private accountService: AuthService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to the api url
        const lang = localStorage.getItem('lang');
        const isApiUrl = request.url.startsWith(environment.apiUrl);
        if (isApiUrl) {
            request = request.clone({
                setHeaders: {
                    'Accept-Language': `${lang != null ? lang : 'en'}`
                }
            });
        }

        return next.handle(request);
    }
}
