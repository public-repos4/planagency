﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, pipe} from 'rxjs';
import {filter, first, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Category} from 'src/app/_models/category/category';
import {SelectOption} from "../_models/select-option";
import {ApiResponse} from "../_models/api-response";

@Injectable({providedIn: 'root'})

export class CategoriesService {

    constructor(
        private http: HttpClient,
    ) {
    }

    add(category: Category) {
        return this.http.post(`${environment.apiUrl}/categories`, category).pipe();
    }

    getAll(params: HttpParams) {
        return this.http.get<ApiResponse>(`${environment.apiUrl}/categories`, {params: params})
            .pipe(map(response => response['data']));
    }

    getById(id: number) {
        return this.getAll(new HttpParams({fromObject: {id: id}}))
            .pipe(map(data => data[0]));
    }

    getSelectOptions(term: string = ''): Observable<SelectOption[]> {
        return this.getAll(new HttpParams({fromObject: {name: term, pageSize: 2000}}))
            .pipe(map(data => data.map(item => ({id: item.id, text: item.name} as SelectOption))))
    }

    edit(id, category: Category) {
        return this.http.put(`${environment.apiUrl}/categories/${id}`, category).pipe();
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/categories/${id}`).pipe();
    }
}
