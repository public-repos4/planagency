import {Injectable} from '@angular/core';
import {HttpRequest, HttpResponse} from '@angular/common/http';

abstract class Cache {
    abstract get(req: HttpRequest<any>): HttpResponse<any> | null;

    abstract put(req: HttpRequest<any>, res: HttpResponse<any>): void;
}

interface CacheEntry {
    url: string;
    response: HttpResponse<any>;
    entryTime: number;
}

@Injectable()
export class CacheService implements Cache {
    cacheMap = new Map<string, CacheEntry>();

    get(req: HttpRequest<any>): HttpResponse<any> | null {
        const entry = this.cacheMap.get(req.urlWithParams);
        return entry ? entry.response : null
    }

    put(req: HttpRequest<any>, res: HttpResponse<any>): void {
        const entry: CacheEntry = {url: req.urlWithParams, response: res, entryTime: Date.now()};
        this.cacheMap.set(req.urlWithParams, entry);
    }

    removeAllCachedRequestsForCustomController(req: HttpRequest<any>): void {
        this.cacheMap.forEach(entry => {
            if (
                (entry.url.includes('categories') && req.url.includes('categories'))
                || (entry.url.includes('products') && req.url.includes('products'))
            ) {
                this.cacheMap.delete(entry.url);
            }
        })
    }
}
