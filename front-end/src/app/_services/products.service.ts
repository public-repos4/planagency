﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, pipe} from 'rxjs';
import {filter, first, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Product} from 'src/app/_models/product/product';
import {SelectOption} from "../_models/select-option";
import {ApiResponse} from "../_models/api-response";
import {ProductSubmit} from "../_models/product/product-submit";

@Injectable({providedIn: 'root'})

export class ProductsService {

    constructor(
        private http: HttpClient,
    ) {
    }

    add(productSubmit: ProductSubmit) {
        // return this.http.post(`${environment.apiUrl}/products`, productSubmit).pipe();

        const formData = new FormData();
        Object.keys(productSubmit).forEach(key => formData.append(key, productSubmit[key]));
        return this.http.post(`${environment.apiUrl}/products`, formData).pipe();
    }

    getAll(params: HttpParams) {
        return this.http.get<ApiResponse>(`${environment.apiUrl}/products`, {params: params})
            .pipe(map(response => response['data']));
    }

    getById(id: number) {
        return this.getAll(new HttpParams({fromObject: {id: id}}))
            .pipe(map(data => data[0]));
    }

    getSelectOptions(term: string = ''): Observable<SelectOption[]> {
        return this.getAll(new HttpParams({fromObject: {name: term, pageSize: 2000}}))
            .pipe(map(data => data.map(item => ({id: item.id, text: item.name} as SelectOption))))
    }

    edit(id, productSubmit: ProductSubmit) {
        // return this.http.put(`${environment.apiUrl}/products/${id}`, productSubmit).pipe();

        const formData = new FormData();
        Object.keys(productSubmit).forEach(key => formData.append(key, productSubmit[key]));
        return this.http.put(`${environment.apiUrl}/products/${id}`, formData).pipe();
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/products/${id}`).pipe();
    }
}
