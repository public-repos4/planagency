﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, pipe} from 'rxjs';
import {filter, first, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {SelectOption} from "../_models/select-option";
import {ApiResponse} from "../_models/api-response";

@Injectable({providedIn: 'root'})

export class EnumsService {

    constructor(
        private http: HttpClient,
    ) {
    }

    getAll(params: HttpParams) {
        return this.http.get<ApiResponse>(`${environment.apiUrl}/enums`, {params: params})
            .pipe(map(response => response['data']));
    }

    getSelectOptions(enumName: string = ''): Observable<SelectOption[]> {
        return this.getAll(new HttpParams({fromObject: {enumName: enumName}}))
            .pipe(map(data => data.map(item => ({id: item.id, text: item.description} as SelectOption))));
    }
}
