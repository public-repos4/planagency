﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AuthGuard} from "./_helpers/auth.guard";

import {LoaderService} from './_services/loader.service';
import {CacheService} from "./_services/cache.service";

import {LoaderInterceptor} from "./_interceptors/loader-interceptor";
import {CachingInterceptor} from "./_interceptors/caching-interceptor";
import {ConfigInterceptor} from './_interceptors/config.interceptor';
import {ErrorInterceptor} from './_interceptors/error.interceptor';
import {JwtInterceptor} from './_interceptors/jwt.interceptor';

import {AppComponent} from './app.component';
import {AlertComponent} from './components/layouts/alert';
import {PreloaderComponent} from './components/layouts/preloader/preloader.component';

const routes: Routes = [
    {
        path: 'admin',
        children: [
            {path: '', loadChildren: () => import('./components/admin-module/admin.module').then(x => x.AdminModule)}
        ],
        canActivate: [AuthGuard]
    },

    {
        path: '',
        children: [
            {path: '', loadChildren: () => import('./components/public-module/public.module').then(x => x.PublicModule)}
        ]
    },
];

@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        PreloaderComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        LoaderService,
        CacheService,

        {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
        // {provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ConfigInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}
