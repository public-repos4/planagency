﻿export class CategoryForm {
    id: number;
    name: string;
    code: string;
    shopId: number;
}
