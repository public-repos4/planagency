﻿export class ProductSubmit {
    id?: number;
    name: string;
    categoryId: string;
    image: any;
    price: number;
    dietaryFlagId: number;
    numOfViews: number;
    description: string;
}
