﻿import {Category} from "../category/category";

export class Product {
    id: number;
    name: string;
    category: Category;
    image: any;
    price: number;
    dietaryFlag: { id: string, text: string, description: string };
    numOfViews: number;
    description: string;
}
