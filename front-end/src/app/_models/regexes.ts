﻿export const regexes = {
    float: '^-?(\\d*)?(\\d*[.])?\\d{1,2}$',
    int: '^[0-9]*$'
};
