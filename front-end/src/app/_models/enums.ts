﻿export enum dietaryFlags {
    VEGAN = 'Vegan',
    LACTOSE_FREE = 'Lactose free',
}

// export const dietaryFlagsPickList = [
//     {id: 'VEGAN', text: 'Vegan'},
//     {id: 'LACTOSE_FREE', text: 'Lactose free'},
// ];

export const dietaryFlagsPickList = [
    {id: 1, text: 'Vegan'},
    {id: 2, text: 'Lactose free'},
];
