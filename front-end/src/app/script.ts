export let toBoolean = function(value) {
    switch (value.toLowerCase()) {
        case true:
        case "true":
        case 1:
        case "1":
        case "on":
        case "yes":
            return true;
        default:
            return false;
    }
}
