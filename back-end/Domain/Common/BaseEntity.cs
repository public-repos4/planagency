﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Domain.Common
{
    public abstract class BaseEntity
    {
        public virtual Guid Id { get; set; }
    }
}