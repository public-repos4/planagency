﻿using System;

namespace Core.Domain.Common
{
    public abstract class AuditableBaseEntity : BaseEntity
    {
        public Guid CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid LastModifiedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
    }
}