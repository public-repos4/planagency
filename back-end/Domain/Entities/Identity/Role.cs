﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Core.Domain.Entities.Identity
{
    public class Role : IdentityRole<Guid>
    {
        public virtual ICollection<UserRole> RoleUsers { get; set; }
    }
}
