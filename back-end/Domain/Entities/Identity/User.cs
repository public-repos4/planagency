﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Core.Domain.Entities.Identity
{
    public class User : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
	}
}
