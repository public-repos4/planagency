﻿using Core.Domain.Common;
using System;
using System.ComponentModel;

namespace Core.Domain.Entities
{
	public class Product : AuditableBaseEntity
	{
		public string Name { get; set; }
		public string Image { get; set; }
		public float Price { get; set; }
		public int DietaryFlagId { get; set; } // DietaryFlags enum
		public long NumOfViews { get; set; }
		public string Description { get; set; }

		public virtual Category Category { get; set; }
		public Guid CategoryId { get; set; }
	}
}