﻿using Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Domain.Entities
{
	public class Category : AuditableBaseEntity
	{
		public string Name { get; set; }

		public virtual Category ParentCategory { get; set; }
		public Guid? ParentCategoryId { get; set; }

		[InverseProperty("ParentCategory")]
		public virtual ICollection<Category> SubCategories { get; set; }

		[InverseProperty("Category")]
		public virtual ICollection<Product> Products { get; set; }

	}
}