﻿using Core.Application.Features.Categories.Commands.CreateCategory;
using Core.Application.Features.Categories.Commands.DeleteCategoryById;
using Core.Application.Features.Categories.Commands.UpdateCategory;
using Core.Application.Features.Categories.Queries.GetCategoryById;
using Core.Application.Features.Categories.Queries.GetCategories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
	[ApiVersion("1.0")]
	public class CategoriesController : BaseApiController
	{
		/// <summary>
		/// POST: api/v1/[controller]
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		/// 
		[HttpPost]
		public async Task<IActionResult> Post([FromForm] CreateCategoryCommand command)
		{
			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// GET: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(Guid id)
		{
			return Ok(await Mediator.Send(new GetCategoryByIdQuery { Id = id }));
		}

		/// <summary>
		/// GET: api/[controller]
		/// </summary>
		/// <param name="queryParams"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<IActionResult> Get([FromQuery] GetCategoriesQuery queryParams)
		{
			return Ok(await Mediator.Send(queryParams));
		}

		/// <summary>
		/// GET: api/v1/[controller]/GetDatatable
		/// </summary>
		/// <param name="dtQueryParams"></param>
		/// <returns></returns>
		[HttpPost]
		[AllowAnonymous]
		[Route("GetDatatable")]
		public async Task<IActionResult> GetDatatable([FromForm] PagedCategoriesQuery dtQueryParams)
		{
			return Ok(await Mediator.Send(dtQueryParams));
		}

		/// <summary>
		/// PUT: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(Guid id, [FromForm] UpdateCategoryCommand command)
		{
			if (id != command.Id)
			{
				return BadRequest();
			}

			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// DELETE: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			return Ok(await Mediator.Send(new DeleteCategoryByIdCommand { Id = id }));
		}
	}
}