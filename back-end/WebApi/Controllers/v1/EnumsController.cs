﻿using Core.Application.Features.Categories.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class EnumsController : BaseApiController
    {
        /// <summary>
        /// GET: api/[controller]
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetEnumsQuery queryParams)
        {
            return Ok(await Mediator.Send(queryParams));
        }
    }
}