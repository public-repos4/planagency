﻿using Core.Application.Features.Products.Queries.GetProducts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Core.Application.Features.Products.Commands.UpdateProduct;
using Core.Application.Features.Products.Queries.GetProductById;
using Core.Application.Features.Products.Commands.CreateProduct;
using Core.Application.Features.Products.Commands.DeleteProductById;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
	[ApiVersion("1.0")]
	public class ProductsController : BaseApiController
	{
		/// <summary>
		/// POST: api/v1/[controller]
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		/// 
		[HttpPost]
		public async Task<IActionResult> Post([FromForm] CreateProductCommand command)
		{
			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// GET: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(Guid id)
		{
			return Ok(await Mediator.Send(new GetProductByIdQuery { Id = id }));
		}

		/// <summary>
		/// GET: api/v1/[controller]
		/// </summary>
		/// <param name="queryParams"></param>
		/// <returns></returns>
		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> Get([FromQuery] GetProductsQuery queryParams)
		{
			return Ok(await Mediator.Send(queryParams));
		}

		/// <summary>
		/// POST: api/v1/[controller]/GetDatatable
		/// </summary>
		/// <param name="dtQueryParams"></param>
		/// <returns></returns>
		[HttpPost]
		[AllowAnonymous]
		[Route("GetDatatable")]
		public async Task<IActionResult> GetDatatable([FromForm] PagedProductsQuery dtQueryParams)
		{
			return Ok(await Mediator.Send(dtQueryParams));
		}

		/// <summary>
		/// PUT: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(Guid id, [FromForm] UpdateProductCommand command)
		{
			if (id != command.Id)
			{
				return BadRequest();
			}

			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// DELETE: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			return Ok(await Mediator.Send(new DeleteProductByIdCommand { Id = id }));
		}
	}
}