﻿using Core.Domain.Entities.Identity;
using Presentation.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Presentation.WebApi.Controllers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Core.Application.Features.Auth.Commands.Login;

namespace PlanAgencyTask.Controllers.v1
{
	[ApiVersion("1.0")]
	public class AuthController : BaseApiController
	{
		private readonly IConfiguration _config;
		private readonly UserManager<User> _usersManager;
		private readonly SignInManager<User> _signInManager;

		public AuthController(IConfiguration config, UserManager<User> usersManager, SignInManager<User> signInManager)
		{
			_usersManager = usersManager;
			_signInManager = signInManager;
			_config = config;
		}

		[HttpPost]
		[AllowAnonymous]
		[Route("Login")]
		public async Task<IActionResult> Login(LoginCommand command)
		{
			return Ok(await Mediator.Send(command));
		}

		//[HttpPost]
		//[AllowAnonymous]
		//[Route("Login")]
		//public async Task<IActionResult> Login(UserCredential userCredential, [FromQuery] string returnUrl = null)
		//{
		//	Admin user = await _usersManager.Users.FirstOrDefaultAsync(user =>
		//		user.UserName == userCredential.UserName.ToLower()
		//   );

		//	if (user == null)
		//		throw new Exception("InvalidLoginAttempt"); // Incorrect credential (details: incorrect Username)

		//	var isCorrect = await _usersManager.CheckPasswordAsync(user, userCredential.Password);

		//	if (!isCorrect)
		//		throw new Exception("InvalidLoginAttempt"); // Incorrect credential (details: correct Username & incorrect password)

		//	return Ok(new
		//	{
		//		Id = user.Id,
		//		FirstName = user.FirstName,
		//		LastName = user.LastName,
		//		Token = GenerateJwtToken(user)
		//	});
		//}

		//[HttpPost]
		//[AllowAnonymous]
		//[Route("Login")]
		//public async Task<IActionResult> Login([FromBody] string userName, [FromBody] string password, [FromQuery] string returnUrl = null)
		//{
		//    Admin user = await _usersManager.Users.FirstOrDefaultAsync(user =>
		//        user.UserName == userName.ToLower()
		//   );

		//    if (user == null)
		//        throw new Exception("InvalidLoginAttempt"); // Incorrect credential (details: incorrect Username)

		//    var isCorrect = await _usersManager.CheckPasswordAsync(user, password);

		//    if (!isCorrect)
		//        throw new Exception("InvalidLoginAttempt"); // Incorrect credential (details: correct Username & incorrect password)

		//    return Ok(new
		//    {
		//        Id = user.Id,
		//        FirstName = user.FirstName,
		//        LastName = user.LastName,
		//        Token = GenerateJwtToken(user)
		//    });
		//}

		private string GenerateJwtToken(User user)
		{
			List<Claim> claims = new()
			{
				new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
				new Claim(ClaimTypes.Name, user.UserName),

				//new Claim(type: "UserId", value: user.Id.ToString()),  //For setting CreatedBy, UpdatedBy and DeletedBy (post, put, patch and delete) 
				//new Claim(type: "UserName", value: user.UserName)
			};

			var roles = _usersManager.GetRolesAsync(user).Result;

			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimTypes.Role, role));
			}

			IEnumerable<Claim> customClaims = _usersManager.GetClaimsAsync(user).Result;

			foreach (var customClaim in customClaims)
			{
				claims.Add(new Claim(customClaim.Type, customClaim.Value));
			}

			SymmetricSecurityKey symmetricSecurityKey = new(Encoding.UTF8.GetBytes(_config.GetSection("SSK").Value));

			SigningCredentials signingCredentials = new(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

			SecurityTokenDescriptor securityTokenDescriptor = new()
			{
				Subject = new ClaimsIdentity(claims),
				Expires = DateTime.Now.AddDays(100),
				SigningCredentials = signingCredentials
			};

			JwtSecurityTokenHandler jwtSecurityTokenHandler = new();

			SecurityToken securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);

			return jwtSecurityTokenHandler.WriteToken(securityToken);
		}
	}
}
