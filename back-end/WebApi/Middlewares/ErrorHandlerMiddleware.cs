﻿using Core.Application.Exceptions;
using Core.Application.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Presentation.WebApi.Middlewares
{
	public class ErrorHandlerMiddleware
	{
		private readonly RequestDelegate _next;

		public ErrorHandlerMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext context)
		{
			try
			{
				await _next(context);
			}
			catch (Exception exception)
			{
				var response = context.Response;
				response.ContentType = "application/json";
				var responseModel = new Response<string>() { Succeeded = false, Message = exception?.Message };


				if (exception?.InnerException != null)
				{
					//if (exception.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
					if (exception.InnerException.Message.Contains("The DELETE statement conflicted with the SAME TABLE REFERENCE constraint"))
					{
						responseModel.MessageKey = "ConflictedWithReferenceConstraint";
						responseModel.Message = "You can not delete this item to its association with other data";
					}
				}

				switch (exception)
				{
					case ApiException e:
						// custom application error
						response.StatusCode = (int)HttpStatusCode.BadRequest;
						break;

					case ValidationException e:
						// custom application error
						response.StatusCode = (int)HttpStatusCode.BadRequest;
						responseModel.Errors = e.Errors;
						break;

					case KeyNotFoundException e:
						// not found error
						response.StatusCode = (int)HttpStatusCode.NotFound;
						break;

					default:
						// unhandled error
						response.StatusCode = (int)HttpStatusCode.InternalServerError;
						break;
				}
				var result = JsonSerializer.Serialize(responseModel);

				await response.WriteAsync(result);
			}
		}
	}
}