﻿using Core.Domain.Entities.Identity;
using IdentityModel;
using IdentityServer4.AccessTokenValidation;
using Infrastructure.Persistence.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Presentation.WebApi.Extensions
{
	public static class ServiceExtensions
	{
		public static void AddSwaggerExtension(this IServiceCollection services)
		{
			services.AddSwaggerGen(c =>
			{
				c.IncludeXmlComments(XmlCommentsFilePath);
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Version = "v1",
					Title = "Plan Agency api - v1",
					Description = "This Api will be responsible for overall data distribution and authorization.",
					Contact = new OpenApiContact
					{
						Name = "Wajeeh Abiad",
						Email = "wajeeh.abiad@gmail.com",
						Url = new Uri("http://wajeehabiad.com/#contact"),
					}
				});
				c.SwaggerDoc("v2", new OpenApiInfo
				{
					Version = "v2",
					Title = "Plan Agency api - v2",
					Description = "This Api will be responsible for overall data distribution and authorization.",
					Contact = new OpenApiContact
					{
						Name = "Wajeeh Abiad",
						Email = "wajeeh.abiad@gmail.com",
						Url = new Uri("http://wajeehabiad.com/#contact"),
					}
				});
				c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
				{
					Name = "Authorization",
					In = ParameterLocation.Header,
					Type = SecuritySchemeType.ApiKey,
					Scheme = "Bearer",
					BearerFormat = "JWT",
					Description = "Input your Bearer token in this format - Bearer {your token here} to access this API",
				});
				c.AddSecurityRequirement(new OpenApiSecurityRequirement
				{
					{
						new OpenApiSecurityScheme
						{
							Reference = new OpenApiReference
							{
								Type = ReferenceType.SecurityScheme,
								Id = "Bearer",
							},
							Scheme = "Bearer",
							Name = "Bearer",
							In = ParameterLocation.Header,
						}, new List<string>()
					},
				});
			});
		}

		public static void AddControllersExtension(this IServiceCollection services)
		{
			services.AddControllers()
				.AddNewtonsoftJson(options =>
				{
					options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				});

			//services.AddControllers()
			//	.AddJsonOptions(options =>
			//	{
			//		options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
			//		options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;

			//		options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
			//		services.AddControllers().AddJsonOptions(x =>
			//	x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
			//	});
		}


		// CORS
		//Configure CORS to allow any origin, header and method. 
		//Change the CORS policy based on your requirements.
		//More info see: https://docs.microsoft.com/en-us/aspnet/core/security/cors?view=aspnetcore-3.0
		public static void AddCorsExtension(this IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy("AllowAll", builder =>
				{
					builder.AllowAnyOrigin()
						   .AllowAnyHeader()
						   .AllowAnyMethod();
				});
			});
		}


		// API version
		public static void AddApiVersioningExtension(this IServiceCollection services)
		{
			services.AddApiVersioning(config =>
			{
				// Specify the default API Version as 1.0
				config.DefaultApiVersion = new ApiVersion(1, 0);
				// If the client hasn't specified the API version in the request, use the default API version number 
				config.AssumeDefaultVersionWhenUnspecified = true;
				// Advertise the API versions supported for the particular endpoint
				config.ReportApiVersions = true;
			});
		}


		// API explorer version
		public static void AddVersionedApiExplorerExtension(this IServiceCollection services)
		{
			services.AddVersionedApiExplorer(o =>
			{
				o.GroupNameFormat = "'v'VVV";
				o.SubstituteApiVersionInUrl = true;
			});
		}


		//API Security
		public static void AddJWTAuthentication(this IServiceCollection services, IConfiguration configuration)
		{

			//services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
			//.AddIdentityServerAuthentication(options =>
			//{
			//    options.Authority = configuration["Sts:ServerUrl"];
			//    options.RequireHttpsMetadata = false;
			//});

			services.AddAuthentication(auth =>
			{
				auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(token =>
			{
				token.RequireHttpsMetadata = false;
				token.SaveToken = true;
				token.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetSection("JWTSettings:Key").Value)),
					ValidateIssuer = false,
					ValidateAudience = false,

					//IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetSection("SSK").Value)),
					//ValidateIssuer = true,
					//ValidateAudience = true,
					//ValidateLifetime = true,
					//ValidateIssuerSigningKey = true,

					//ValidIssuer= "https://localhost:4011",
					//ValidAudience= "https://localhost:4011",
					//IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetSection("SSK").Value)),
				};
			});
		}
		public static void AddGlobalAuthorization(this IServiceCollection services)
		{
			services.AddControllers(options =>
			{
				//Apply authorize attribute globally
				//var policy = new AuthorizationPolicyBuilder("Bearer")
				var policy = new AuthorizationPolicyBuilder()
					.AddAuthenticationSchemes("Bearer")
					.RequireAuthenticatedUser()
					.Build();
				options.Filters.Add(new AuthorizeFilter(policy));
			});
		}

		public static void AddAuthorizationPolicies(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddAuthorization(options =>
			{
				options.AddPolicy("AddingShops", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "AddShops"); });
				options.AddPolicy("ReadingShops", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "ReadShops"); });
				options.AddPolicy("EditingShops", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "EditShops"); });
				options.AddPolicy("DeletingShops", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "DeleteShops"); });

				//options.AddPolicy("AddingShops", policy => policy.RequireClaim("AppPermissions", "AddShops"));
				//options.AddPolicy("ReadingShops", policy => policy.RequireClaim("AppPermissions", "ReadShops"));
				//options.AddPolicy("EditingShops", policy => policy.RequireClaim("AppPermissions", "EditShops"));
				//options.AddPolicy("DeletingShops", policy => policy.RequireClaim("AppPermissions", "DeleteShops"));

				//options.AddPolicy("AddingShops", policy => policy.RequireRole("Admin"));
				//options.AddPolicy("ReadingShops", policy => policy.RequireRole("Admin"));
				//options.AddPolicy("EditingShops", policy => policy.RequireRole("Admin"));
				//options.AddPolicy("DeletingShops", policy => policy.RequireRole("Admin"));
			});
		}


		//public static void AddAuthorizationPolicies(this IServiceCollection services, IConfiguration configuration)
		//{
		//    string hradmin = configuration["ApiRoles:HRAdminRole"],
		//            manager = configuration["ApiRoles:ManagerRole"], 
		//            employee = configuration["ApiRoles:EmployeeRole"];

		//    services.AddAuthorization(options =>
		//    {
		//        options.AddPolicy(AuthorizationConsts.HrAdminPolicy, policy => policy.RequireAssertion(context => HasRole(context.Admin, hradmin)));
		//        options.AddPolicy(AuthorizationConsts.ManagerPolicy, policy => policy.RequireAssertion(context => HasRole(context.Admin, manager) || HasRole(context.Admin, hradmin)));
		//        options.AddPolicy(AuthorizationConsts.EmployeePolicy, policy => policy.RequireAssertion(context => HasRole(context.Admin, employee) || HasRole(context.Admin, manager) || HasRole(context.Admin, hradmin)));
		//    });
		//}


		//API Identity
		public static void AddIdentity(this IServiceCollection services)
		{
			//services.AddIdentity<Admin, Role>(options =>
			//{
			//	options.Password.RequiredLength = 1;
			//	options.Password.RequireNonAlphanumeric = false;
			//	options.Password.RequireDigit = false;
			//	options.Password.RequireUppercase = false;
			//	options.Password.RequireLowercase = false;
			//}).AddEntityFrameworkStores<AppDbContext>();

			// ===== Add Identity ========
			services.AddIdentity<User, Role>(options =>
			{
				options.Password.RequiredLength = 1;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireDigit = false;
				options.Password.RequireUppercase = false;
				options.Password.RequireLowercase = false;
			}).AddEntityFrameworkStores<AppDbContext>();

			//services.Configure<DataProtectionTokenProviderOptions>(options =>
			//	options.TokenLifespan = TimeSpan.FromHours(3)
			//);

			//===== Configure Identity =======
			//services.ConfigureApplicationCookie(options =>
			//{
			//	//options.Cookie.Name = "auth_cookie";
			//	//options.Cookie.SameSite = SameSiteMode.None;
			//	//options.LoginPath = new PathString("/api/contests");
			//	////options.LoginPath = new PathString("");
			//	//options.AccessDeniedPath = new PathString("/api/contests");

			//	//options.Cookie.Name = "auth_cookie";
			//	//options.Cookie.SameSite = SameSiteMode.None;
			//	options.LoginPath = string.Empty;
			//	//options.LoginPath = new PathString("");
			//	options.AccessDeniedPath = string.Empty;

			//	// Not creating a new object since ASP.NET Identity has created
			//	// one already and hooked to the OnValidatePrincipal event.
			//	// See https://github.com/aspnet/AspNetCore/blob/5a64688d8e192cacffda9440e8725c1ed41a30cf/src/Identity/src/Identity/IdentityServiceCollectionExtensions.cs#L56

			//	options.Events.OnRedirectToLogin = context =>
			//	{
			//		context.Response.StatusCode = StatusCodes.Status401Unauthorized;
			//		return Task.CompletedTask;
			//	};
			//});


			//.AddCookie((o) =>
			//{
			//	o.Cookie.HttpOnly = true;
			//	o.LoginPath = string.Empty;
			//	o.AccessDeniedPath = string.Empty;
			//	o.Events.OnRedirectToLogin = context =>
			//	{
			//		context.Response.StatusCode = StatusCodes.Status401Unauthorized;
			//		return Task.CompletedTask;
			//	};
			//});

		}


		public static bool HasRole(ClaimsPrincipal user, string role)
		{
			if (string.IsNullOrEmpty(role))
				return false;

			return user.HasClaim(c =>
								(c.Type == JwtClaimTypes.Role || c.Type == $"client_{JwtClaimTypes.Role}") &&
								System.Array.Exists(c.Value.Split(','), e => e == role)
							);
		}

		static string XmlCommentsFilePath
		{
			get
			{
				var basePath = PlatformServices.Default.Application.ApplicationBasePath;
				var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
				return Path.Combine(basePath, fileName);
			}
		}

	}

}
