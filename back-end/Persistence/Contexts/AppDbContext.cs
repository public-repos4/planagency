﻿using Core.Application.Interfaces;
using Core.Domain.Common;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Contexts
{
	//public class AppDbContext : IdentityDbContext
	public class AppDbContext :
		IdentityDbContext<User, Role, Guid,
		UserClaim, UserRole, UserLogin,
		RoleClaim, UserToken>
	{
		private readonly IDateTimeService _dateTime;
		private readonly ILoggerFactory _loggerFactory;
		protected readonly IHttpContextAccessor _httpContextAccessor;

		public AppDbContext(DbContextOptions<AppDbContext> options,
		  IDateTimeService dateTime,
		  ILoggerFactory loggerFactory,
		  IHttpContextAccessor httpContextAccessor = null
		  ) : base(options)
		{
			ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking; // Must be commented on seeding the database
			_dateTime = dateTime;
			_loggerFactory = loggerFactory;
			_httpContextAccessor = httpContextAccessor;
		}

		public DbSet<Category> Categories { get; set; }
		public DbSet<Product> Products { get; set; }


		public DbSet<User> Admins { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<Category>(category =>
			{
				category.HasOne(category => category.ParentCategory)
					.WithMany(parentCategory => parentCategory.SubCategories)
					.OnDelete(DeleteBehavior.Restrict);
			});

			builder.Entity<Product>(product=>
			{
				product.HasOne(product => product.Category)
					.WithMany(category => category.Products)
					.OnDelete(DeleteBehavior.Restrict);
			});

			#region Entities


			#region Users
			builder.Entity<User>(entity =>
			{
				entity.ToTable("Users");
			});
			#endregion

			#region Roles
			builder.Entity<Role>(entity =>
			{
				entity.ToTable("Roles");
			});
			#endregion

			#region UsersRoles
			builder.Entity<UserRole>(entity =>
			{
				entity.ToTable("UsersRoles");
			});
			#endregion

			#region UserLogins
			builder.Entity<UserLogin>(entity =>
			{
				entity.ToTable("UserLogins");
			});
			#endregion

			#region UserTokens
			builder.Entity<UserToken>(entity =>
			{
				entity.ToTable("UserTokens");
			});
			#endregion

			#region UserClaims
			builder.Entity<UserClaim>(entity =>
			{
				entity.ToTable("UserClaims");
			});
			#endregion

			#region RolesClaims
			builder.Entity<RoleClaim>(entity =>
			{
				entity.ToTable("RolesClaims");
			});
			#endregion


			#region Categories
			//builder.Entity<Category>(entity =>
			//{
			//	entity.Property(entity => entity.Id).HasDefaultValueSql("NEWID()");
			//});
			#endregion


			#endregion

			#region Views
			#endregion
		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		{
			var x = ChangeTracker.Entries<AuditableBaseEntity>();

			foreach (var entry in ChangeTracker.Entries<AuditableBaseEntity>())
			{
				switch (entry.State)
				{
					case EntityState.Added:
						entry.Entity.CreatedAt = _dateTime.NowUtc;
						break;

					case EntityState.Modified:
						entry.Entity.LastModifiedAt = _dateTime.NowUtc;
						break;
				}
			}
			return base.SaveChangesAsync(cancellationToken);
		}

		//public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		//{
		//	var entries = ChangeTracker
		//		.Entries()
		//		.Where(e => e.Entity is AuditableBaseEntity && (e.State == EntityState.Added || e.State == EntityState.Modified));

		//	int userId = int.Parse(_httpContextAccessor.HttpContext.Admin.FindFirst(ClaimTypes.NameIdentifier).Value);

		//	foreach (var entityEntry in entries)
		//	{
		//		if (entityEntry.State == EntityState.Added)
		//		{
		//			((AuditableBaseEntity)entityEntry.Entity).CreatedAt = DateTime.UtcNow;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedAt = DateTime.UtcNow;

		//			((AuditableBaseEntity)entityEntry.Entity).CreatedBy = userId;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedBy = userId;
		//		}

		//		if (entityEntry.State == EntityState.Modified)
		//		{
		//			entityEntry.Property("CreatedAt").IsModified = false;
		//			entityEntry.Property("CreatedBy").IsModified = false;

		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedAt = DateTime.UtcNow;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedBy = userId;
		//		}
		//	}

		//	return base.SaveChangesAsync(cancellationToken);
		//}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			//optionsBuilder.EnableSensitiveDataLogging();
			optionsBuilder.UseLoggerFactory(_loggerFactory);
			//optionsBuilder.UseLazyLoadingProxies();
		}
	}
}