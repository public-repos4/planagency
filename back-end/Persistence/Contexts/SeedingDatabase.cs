﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Collections.Generic;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Bogus;

namespace Infrastructure.Persistence.Contexts
{
	public class SeedingDatabase
	{
		public static Faker<Category> CategoryFaker;
		public static Faker<Product> ProductFaker;

		public async static Task Seed(IServiceProvider serviceProvider)
		{
			var appDbContext = serviceProvider.GetRequiredService<AppDbContext>();
			var usersManager = serviceProvider.GetRequiredService<UserManager<User>>();
			var rolesManager = serviceProvider.GetRequiredService<RoleManager<Role>>();

			try
			{
				appDbContext.Database.OpenConnection();

				if (!rolesManager.Roles.Any())
				{
					var roles = new List<Role>
					{
						new Role{Name = "Super admin"},
						new Role{Name = "Admin"},
						new Role{Name = "Super user"},
						new Role{Name = "User"},
					};

					foreach (var role in roles)
					{
						await rolesManager.CreateAsync(role);
					}
				}

				if (!usersManager.Users.Any())
				{
					var adminUser = new User
					{
						FirstName = "admin",
						LastName = "admin",
						UserName = "admin",
						Password = "admin",
						Email = "admin@test.org",
					};

					IdentityResult result;

					result = usersManager.CreateAsync(adminUser, adminUser.Password).Result;
					if (result.Succeeded)
					{
						var admin = usersManager.FindByNameAsync(adminUser.UserName).Result;

						// To prevent throwing tracking exception on addin roles to this admin (user)
						appDbContext.ChangeTracker.Entries<User>().Where(x => x.Entity.Id == admin.Id).First().State = EntityState.Detached;

						await usersManager.AddToRolesAsync(admin, new[] { "Admin" });
					}

					var wajeehUser = new User
					{
						FirstName = "Wajeeh",
						LastName = "Abiad",
						UserName = "wajeeh",
						Password = "wajeeh",
						Email = "wajeeh@test.org",
					};

					result = usersManager.CreateAsync(wajeehUser, wajeehUser.Password).Result;
					if (result.Succeeded)
					{
						var wajeeh = usersManager.FindByNameAsync(wajeehUser.UserName).Result;

						// To prevent throwing tracking exception on addin roles to this admin (user)
						appDbContext.ChangeTracker.Entries<User>().Where(x => x.Entity.Id == wajeeh.Id).First().State = EntityState.Detached;

						await usersManager.AddToRolesAsync(wajeeh, new[] { "User" });
					}
				}

				if (!appDbContext.Categories.Any())
				{
					for (int i = 0; i < 100; i++)
					{
						var categories = appDbContext.Categories.ToList();

						appDbContext.Categories.Add(
							new Faker<Category>()
							.RuleFor(o => o.Name, f => f.Commerce.Categories(1).First())
							.RuleFor(o => o.ParentCategoryId, f => appDbContext.Categories.ToList().Count > 0 ? appDbContext.Categories.ToList()[f.Random.Number(0, appDbContext.Categories.ToList().Count - 1)].Id : null)
							.Generate(1).First()
						);

						await appDbContext.SaveChangesAsync();
					}
				}

				//if (!appDbContext.Products.Any())
				//{
				//	var categories = appDbContext.Categories.ToList();

				//	ProductFaker = new Faker<Product>()
				//	.RuleFor(o => o.Name, f => f.Commerce.Product())
				//	.RuleFor(o => o.Description, f => f.Commerce.ProductDescription())
				//	.RuleFor(o => o.Price, f => float.Parse(f.Commerce.Price()))
				//	//.RuleFor(o => o.Image, f => f.Image.Animals())
				//	.RuleFor(o => o.NumOfViews, f => f.Random.Number(0, 10000))
				//	.RuleFor(o => o.DietaryFlag, f => Enum.GetValues(typeof(DietaryFlags)).GetValue(f.Random.Number(0, Enum.GetValues(typeof(DietaryFlags)).Length - 1)))
				//	.RuleFor(o => o.CategoryId, f => categories[f.Random.Number(0, categories.Count - 1)].Id);

				//	appDbContext.Products.AddRange(ProductFaker.Generate(100));
				//	await appDbContext.SaveChangesAsync();
				//}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				appDbContext.Database.CloseConnection();
			}
		}
	}
}
