﻿using Core.Application.Features.Products.Queries.GetProducts;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Infrastructure.Persistence.Contexts;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class ProductRepositoryAsync : GenericRepositoryAsync<Product>, IProductRepositoryAsync
	{
		private readonly DbSet<Product> _products;

		public ProductRepositoryAsync(AppDbContext dbContext) : base(dbContext)
		{
			_products = dbContext.Set<Product>();
		}

		public async Task<(IEnumerable<Product> data, RecordsCount recordsCount)> GetPagedProductResponseAsync(GetProductsQuery queryParams)
		{
			var orderBy = queryParams.OrderBy;
			int recordsTotal, recordsFiltered;

			// Setup IQueryable
			var query = _products
				.AsNoTracking()
				.Include("Category")
				.AsExpandable();

			// Count records total
			recordsTotal = await query.CountAsync();

			// filter data
			query = FilterData(query, queryParams);

			// Count records after filter
			recordsFiltered = await query.CountAsync();

			//set Record counts
			var recordsCount = new RecordsCount
			{
				Filtered = recordsFiltered,
				Total = recordsTotal
			};

			// set order by
			if (!string.IsNullOrWhiteSpace(orderBy))
			{
				query = query.OrderBy(orderBy);
			}

			// paginate data
			query = PaginateData(query, queryParams.PageNumber, queryParams.PageSize);

			// retrieve data to list
			var resultData = await query.ToListAsync();

			// shape data
			//var shapeData = _dataShaper.ShapeData(resultData, fields);

			return (resultData, recordsCount);
		}

		private static IQueryable<Product> FilterData(IQueryable<Product> products, GetProductsQuery queryParams)
		{
			var id = queryParams.Id;
			var ids = queryParams.Ids;
			var search = queryParams.Search?.Trim();
			var name = queryParams.Name?.Trim();

			var predicate = PredicateBuilder.New<Product>(true);

			if (!string.IsNullOrEmpty(id.ToString()))
			{
				predicate = predicate.Or(entity => entity.Id == id);
				products.Where(predicate);
			}

			if (ids != null)
			{
				predicate = predicate.Or(entity => ids.Contains(entity.Id));
				products.Where(predicate);
			}

			if (!string.IsNullOrEmpty(search))
			{
				predicate = predicate
							.Or(product => product.Name.Contains(search))
							.Or(product => product.Price.ToString().Contains(search))
							.Or(product => product.NumOfViews.ToString().Contains(search))
							.Or(product => product.Description.Contains(search));
			}

			products = products.Where(predicate);

			return products;
		}
	}
}