﻿using Core.Application.Features.Categories.Queries.GetCategories;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Infrastructure.Persistence.Contexts;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class CategoryRepositoryAsync : GenericRepositoryAsync<Category>, ICategoryRepositoryAsync
	{
		private readonly DbSet<Category> _categories;

		public CategoryRepositoryAsync(AppDbContext dbContext) : base(dbContext)
		{
			_categories = dbContext.Set<Category>();
		}

		public async Task<(IEnumerable<Category> data, RecordsCount recordsCount)> GetPagedCategoryResponseAsync(GetCategoriesQuery queryParams)
		{
			var orderBy = queryParams.OrderBy;
			int recordsTotal, recordsFiltered;

			// Setup IQueryable
			var query = _categories
				.AsNoTracking()
				.Include("ParentCategory")
				.Include("SubCategories")
				.AsExpandable();

			// Count records total
			recordsTotal = await query.CountAsync();

			// filter data
			query = FilterData(query, queryParams);

			// Count records after filter
			recordsFiltered = await query.CountAsync();

			//set Record counts
			var recordsCount = new RecordsCount
			{
				Filtered = recordsFiltered,
				Total = recordsTotal
			};

			// set order by
			if (!string.IsNullOrWhiteSpace(orderBy))
			{
				query = query.OrderBy(orderBy);
			}

			// paginate data
			query = PaginateData(query, queryParams.PageNumber, queryParams.PageSize);

			// retrieve data to list
			var resultData = await query.ToListAsync();

			return (resultData, recordsCount);
		}

		private static IQueryable<Category> FilterData(IQueryable<Category> categories, GetCategoriesQuery queryParams)
		{
			var id = queryParams.Id;
			var ids = queryParams.Ids;
			var search = queryParams.Search?.Trim();

			var name = queryParams.Name?.Trim();

			var predicate = PredicateBuilder.New<Category>(true);

			if (!string.IsNullOrEmpty(id.ToString()))
			{
				predicate = predicate.Or(entity => entity.Id == id);
				categories.Where(predicate);
			}

			if (ids != null)
			{
				predicate = predicate.Or(entity => ids.Contains(entity.Id));
				categories.Where(predicate);
			}

			if (!string.IsNullOrEmpty(search))
			{
				predicate = predicate
							.Or(entity => entity.Name.Contains(search));
			}

			if (!string.IsNullOrEmpty(name))
				predicate = predicate.Or(entity => entity.Name.Contains(name));

			categories = categories.Where(predicate);

			return categories;
		}
	}
}