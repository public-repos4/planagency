﻿using Core.Application.Interfaces;
using Infrastructure.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class GenericRepositoryAsync<T> : IGenericRepositoryAsync<T> where T : class
	{
		private readonly AppDbContext _dbContext;

		public GenericRepositoryAsync(AppDbContext dbContext)
		{
			_dbContext = dbContext;
		}
		public async Task<T> AddAsync(T entity)
		{
			await _dbContext.Set<T>().AddAsync(entity);
			await _dbContext.SaveChangesAsync();
			return entity;
		}

		public virtual async Task<T> GetByIdAsync(Guid id)
		{
			return await _dbContext.Set<T>().FindAsync(id);
		}

		public async Task<IEnumerable<T>> GetAllAsync()
		{
			return await _dbContext.Set<T>().ToListAsync();
		}

		public async Task<IEnumerable<T>> GetPagedReponseAsync(int pageNumber, int pageSize)
		{
			// Setup IQueryable
			var query = _dbContext
				.Set<T>()
				.AsNoTracking();

			return await PaginateData(query, pageNumber, pageSize).ToListAsync();
		}

		public async Task<IEnumerable<T>> GetPagedAdvancedReponseAsync(int pageNumber, int pageSize, string orderBy, string fields)
		{
			// Setup IQueryable
			var query = _dbContext
				.Set<T>()
				.Select<T>("new(" + fields + ")")
				.OrderBy(orderBy)
				.AsNoTracking();

			return await PaginateData(query, pageNumber, pageSize).ToListAsync();
		}

		public IQueryable<T> PaginateData(IQueryable<T> query, int? pageNumber, int? pageSize)
		{
			if (pageNumber != null && pageSize != null)
			{
				if (pageNumber == -1) //return the last page
				{
					query = query
						.Skip(Math.Max(0, query.Count() - query.Count() % pageSize.Value))
						.Take(query.Count() % pageSize.Value);
				}
				else //return a custom page
				{
					int skip = (pageNumber.Value - 1) * pageSize.Value;
					query = query.Skip(skip).Take(pageSize.Value);
				}
			}

			return query;
		}

		public async Task UpdateAsync(T entity)
		{
			_dbContext.Update(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(T entity)
		{
			_dbContext.Set<T>().Remove(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task Commit()
		{
			await _dbContext.SaveChangesAsync();
		}
	}
}