﻿using Core.Domain.Entities;
using AutoMapper;
using Core.Application.Features.Products.Queries.GetProducts;
using Core.Application.Features.Categories.Queries.GetCategories;
using Core.Application.Features.Categories.Commands.CreateCategory;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Core.Application.Features.Categories.Commands.UpdateCategory;
using Core.Application.Features.Products.Commands.CreateProduct;
using Core.Application.Features.Products.Commands.UpdateProduct;
//using System.Web.Mvc.Html;
using System;
using System.Reflection;
using System.ComponentModel;
using Core.Application.Enums;
using Core.Application.Helpers;
using Core.Application.Interfaces;

namespace Core.Application.Mappings
{
	public class GeneralProfile : Profile
	{

		public GeneralProfile()
		{

			CreateMap<CreateCategoryCommand, Category>();
			CreateMap<UpdateCategoryCommand, Category>();
			CreateMap<Category, GetCategoryDto>().ReverseMap();

			CreateMap<CreateProductCommand, Product>();
			CreateMap<UpdateProductCommand, Product>();
			CreateMap<Product, GetProductDto>()
			.ForMember(x => x.DietaryFlag, opt => opt.Ignore())
			//.ForMember(dest => dest.DietaryFlag, opts => opts.MapFrom(src => _enumHelper.GetEnumValueAsObject<DietaryFlags>(src.DietaryFlag)))
			//.ForMember(dest => dest.DietaryFlag, opts => opts.MapFrom(src => _enumHelper.GetEnumValueAsObject<DietaryFlags>(src.DietaryFlag)))
			.AfterMap<RefactoreImagesPath>()
			.AfterMap<DietaryFlagEnumValueToObject>();
		}

		public class BaseMappingAction
		{
			private readonly IHttpContextAccessor _httpContextAccessor;

			public BaseMappingAction(IHttpContextAccessor httpContextAccessor)
			{
				_httpContextAccessor = httpContextAccessor;
			}

			public string GetServerPath()
			{
				return _httpContextAccessor.HttpContext.Request.IsHttps
				   ? "https://" + _httpContextAccessor.HttpContext.Request.Host.ToString()
				   : "http://" + _httpContextAccessor.HttpContext.Request.Host.ToString();
			}

			public string GetCulture()
			{
				List<string> allowdCultures = new() { "en", "ar" };

				IHeaderDictionary headers = _httpContextAccessor.HttpContext.Request.Headers;

				string culture = "en";

				if (headers.ContainsKey("Accept-Language"))
				{
					if (allowdCultures.Contains(headers["Accept-Language"].ToString()))
					{
						culture = headers["Accept-Language"].ToString();
					}
				}

				return culture;
			}
		}

		public class RefactoreImagesPath : BaseMappingAction, IMappingAction<Product, GetProductDto>
		{
			public RefactoreImagesPath(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor) { }

			public void Process(Product source, GetProductDto destination, ResolutionContext context)
			{
				string ServerPath = GetServerPath();
				destination.Image = $"{ServerPath}{source.Image}";
			}
		}

		public class DietaryFlagEnumValueToObject : IMappingAction<Product, GetProductDto>
		{
			private readonly IEnumHelper _enumHelper;

			public DietaryFlagEnumValueToObject(IEnumHelper enumHelper)
			{
				_enumHelper = enumHelper;
			}

			public void Process(Product source, GetProductDto destination, ResolutionContext context)
			{
				destination.DietaryFlag = _enumHelper.GetEnumValueAsObject<DietaryFlags>(source.DietaryFlagId);
			}
		}

		public static string GetEnumDescription(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());

			DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

			if (attributes != null && attributes.Length > 0)
				return attributes[0].Description;
			else
				return value.ToString();
		}
	}
}

