﻿using System;

namespace Core.Application.Enums
{
    public class GetEnumDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}