﻿
using System.ComponentModel;

namespace Core.Application.Enums
{
	public enum DietaryFlags
	{
		[Description("Vegan")]
		Vegan = 1,

		[Description("Lactose free")]
		LactoseFree = 2
	}
}