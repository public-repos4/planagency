﻿using Core.Application.Enums;
using Core.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Core.Application.Helpers
{
	public class EnumHelper : IEnumHelper
	{
		/// <summary>
		/// Get list of field names in the model class
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		/// <returns></returns>
		public IEnumerable<GetEnumDto> GetEnumAsList<TEnum>()
		{
			return Enum.GetValues(typeof(TEnum))
						.Cast<TEnum>()
						.Select(value => new GetEnumDto { Id = (int)(object)value, Text = value.ToString(), Description = GetEnumDescription((Enum)(object)value) })
						.ToList();
		}

		/// <summary>
		/// Get list of field names in the model class
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		/// <typeparam name="value"></typeparam>
		/// <returns></returns>
		public GetEnumDto GetEnumValueAsObject<TEnum>(int value)
		{
			return new GetEnumDto
			{
				Id = (int)(object)value,
				Text = ((TEnum)(object)value).ToString(),
				Description = GetEnumDescription((Enum)(object)((TEnum)(object)value))
			};
		}

		private string GetEnumDescription(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());

			DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

			if (attributes != null && attributes.Length > 0)
				return attributes[0].Description;
			else
				return value.ToString();
		}
	}
}