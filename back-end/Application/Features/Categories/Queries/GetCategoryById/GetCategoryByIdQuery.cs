﻿using AutoMapper;
using Core.Application.Exceptions;
using Core.Application.Features.Categories.Queries.GetCategories;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Categories.Queries.GetCategoryById
{
	public class GetCategoryByIdQuery : IRequest<Response<GetCategoryDto>>
	{
		public Guid Id { get; set; }

		public class GetCategoryByIdQueryHandler : IRequestHandler<GetCategoryByIdQuery, Response<GetCategoryDto>>
		{
			private readonly ICategoryRepositoryAsync _categoryRepository;
			private readonly IMapper _mapper;

			public GetCategoryByIdQueryHandler(ICategoryRepositoryAsync categoryRepository, IMapper mapper)
			{
				_categoryRepository = categoryRepository;
				_mapper = mapper;
			}

			public async Task<Response<GetCategoryDto>> Handle(GetCategoryByIdQuery queryParams, CancellationToken cancellationToken)
			{
				var category = await _categoryRepository.GetByIdAsync(queryParams.Id);
				if (category == null) throw new ApiException($"Category Not Found.");
				return new Response<GetCategoryDto>(_mapper.Map<GetCategoryDto>(category));
			}
		}
	}
}