﻿using System;

namespace Core.Application.Features.Categories.Queries.GetCategories
{
    public class GetCategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public GetCategoryDto ParentCategory { get; set; }
    }
}