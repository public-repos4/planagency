﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Categories.Queries.GetCategories
{
	public class GetCategoriesQuery : QueryParameter, IRequest<PagedResponse<IEnumerable<Entity>>>
	{
		public string Name { get; set; }
	}

	public class GetAllCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, PagedResponse<IEnumerable<Entity>>>
	{
		private readonly ICategoryRepositoryAsync _categoryRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetCategoryDto> _dataShaper;
		private readonly IMapper _mapper;

		public GetAllCategoriesQueryHandler(
			ICategoryRepositoryAsync categoryRepository,
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetCategoryDto> dataShaper)
		{
			_categoryRepository = categoryRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedResponse<IEnumerable<Entity>>> Handle(GetCategoriesQuery queryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = queryParams;

			//filtered fields security
			if (!string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//limit to fields in view model
				validQueryParams.Fields = _modelHelper.ValidateModelFields<GetCategoryDto>(validQueryParams.Fields);
			}
			else
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetCategoryDto>();
			}

			// query based on filter
			var result = await _categoryRepository.GetPagedCategoryResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetCategoryDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			// shape data
			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedResponse<IEnumerable<Entity>>(shapeData, validQueryParams.PageNumber, validQueryParams.PageSize, recordCount);
		}
	}
}