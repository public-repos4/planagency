﻿using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Categories.Commands.DeleteCategoryById
{
    public class DeleteCategoryByIdCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }

        public class DeleteCategoryByIdCommandHandler : IRequestHandler<DeleteCategoryByIdCommand, Response<Guid>>
        {
            private readonly ICategoryRepositoryAsync _categoryRepository;

            public DeleteCategoryByIdCommandHandler(ICategoryRepositoryAsync categoryRepository)
            {
                _categoryRepository = categoryRepository;
            }

            public async Task<Response<Guid>> Handle(DeleteCategoryByIdCommand command, CancellationToken cancellationToken)
            {
                var category = await _categoryRepository.GetByIdAsync(command.Id);
                if (category == null) throw new ApiException($"Category Not Found.");
                await _categoryRepository.DeleteAsync(category);
                return new Response<Guid>(category.Id, "SucceededDelete");
            }   
        }
    }
}