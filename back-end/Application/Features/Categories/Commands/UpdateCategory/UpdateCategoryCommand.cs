﻿using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Categories.Commands.UpdateCategory
{
	public class UpdateCategoryCommand : IRequest<Response<Guid>>
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public Guid? ParentCategoryId { get; set; }
	}
	public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, Response<Guid>>
	{
		private readonly ICategoryRepositoryAsync _categoryRepository;

		public UpdateCategoryCommandHandler(ICategoryRepositoryAsync categoryRepository)
		{
			_categoryRepository = categoryRepository;
		}

		public async Task<Response<Guid>> Handle(UpdateCategoryCommand command, CancellationToken cancellationToken)
		{
			var category = await _categoryRepository.GetByIdAsync(command.Id);

			if (category == null)
			{
				throw new ApiException($"Category Not Found.");
			}
			else
			{
				category.Name = command.Name;
				category.ParentCategoryId = command.ParentCategoryId;
				await _categoryRepository.UpdateAsync(category);
				return new Response<Guid>(category.Id, "SucceededUpdate");
			}
		}
	}
}