﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Core.Application.Enums;

namespace Core.Application.Features.Categories.Queries
{
	public class GetEnumsQuery : IRequest<PagedResponse<IEnumerable<GetEnumDto>>>
	{
		public string EnumName { get; set; }
	}

	public class GetEnumsQueryHandler : IRequestHandler<GetEnumsQuery, PagedResponse<IEnumerable<GetEnumDto>>>
	{
		//private readonly IModelHelper _modelHelper;
		//private readonly IDataShapeHelper<GetEnumDto> _dataShaper;
		//private readonly IMapper _mapper;
		private readonly IEnumHelper _enumHelper;

		public GetEnumsQueryHandler(
			//ICategoryRepositoryAsync categoryRepository,
			//IModelHelper modelHelper,
			//IMapper mapper,
			IEnumHelper enumHelper)
		{
			//_categoryRepository = categoryRepository;
			//_modelHelper = modelHelper;
			//_mapper = mapper;
			_enumHelper = enumHelper;
		}

		public async Task<PagedResponse<IEnumerable<GetEnumDto>>> Handle(GetEnumsQuery queryParams, CancellationToken cancellationToken)
		{
			IEnumerable<GetEnumDto> listedEnum = queryParams.EnumName.ToLower() switch
			{
				"dietary_flags" => _enumHelper.GetEnumAsList<DietaryFlags>(),
				_ => new List<GetEnumDto>()
			};

			return new PagedResponse<IEnumerable<GetEnumDto>>(listedEnum, 1, listedEnum.Count(), new RecordsCount { Filtered = listedEnum.Count(), Total = listedEnum.Count() });
		}
	}
}