﻿using Core.Application.Enums;
using Core.Application.Features.Categories.Queries.GetCategories;
using System;

namespace Core.Application.Features.Products.Queries.GetProducts
{
    public class GetProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public string Image { get; set; }
        public long NumOfViews { get; set; }
        public GetEnumDto DietaryFlag { get; set; }
        public GetCategoryDto Category { get; set; }
    }
}