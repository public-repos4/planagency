﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Products.Queries.GetProducts
{
	public class GetProductsQuery : QueryParameter, IRequest<PagedResponse<IEnumerable<Entity>>>
	{
		public string Name { get; set; }
	}

	public class GetAllProductsQueryHandler : IRequestHandler<GetProductsQuery, PagedResponse<IEnumerable<Entity>>>
	{
		private readonly IProductRepositoryAsync _productRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetProductDto> _dataShaper;
		private readonly IMapper _mapper;

		public GetAllProductsQueryHandler(
			IProductRepositoryAsync productRepository, 
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetProductDto> dataShaper)
		{
			_productRepository = productRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedResponse<IEnumerable<Entity>>> Handle(GetProductsQuery queryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = queryParams;

			//filtered fields security
			if (!string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//limit to fields in view model
				validQueryParams.Fields = _modelHelper.ValidateModelFields<GetProductDto>(validQueryParams.Fields);
			}
			else
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetProductDto>();
			}

			// query based on filter
			var result = await _productRepository.GetPagedProductResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetProductDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			// shape data
			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedResponse<IEnumerable<Entity>>(shapeData, validQueryParams.PageNumber, validQueryParams.PageSize, recordCount);
		}
	}
}