﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Parameters.Datatable;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Products.Queries.GetProducts
{
	public partial class PagedProductsQuery : IRequest<PagedDataTableResponse<IEnumerable<Entity>>>
	{
		//strong type input parameters
		public int Draw { get; set; } //page number
		public int Start { get; set; } //Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the first record).
		public int Length { get; set; } //page size
		public IList<Order> Order { get; set; } //Order by
		public Search Search { get; set; } //search criteria
		public IList<Column> Columns { get; set; } //select fields
	}

	public class PageProductQueryHandler : IRequestHandler<PagedProductsQuery, PagedDataTableResponse<IEnumerable<Entity>>>
	{
		private readonly IProductRepositoryAsync _productRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetProductDto> _dataShaper;
		private readonly IMapper _mapper;

		public PageProductQueryHandler(
			IProductRepositoryAsync productRepository,
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetProductDto> dataShaper)

		{
			_productRepository = productRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedDataTableResponse<IEnumerable<Entity>>> Handle(PagedProductsQuery dtQueryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = new GetProductsQuery
			{
				// Draw map to PageNumber
				PageNumber = (dtQueryParams.Start / dtQueryParams.Length) + 1,
				// Length map to PageSize
				PageSize = dtQueryParams.Length
			};

			// Map order > OrderBy
			var colOrder = dtQueryParams.Order[0];
			switch (colOrder.Column)
			{
				case 0:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Id" : "Id DESC";
					break;

				case 1:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Name" : "Name DESC";
					break;

				case 4:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Price" : "Price DESC";
					break;

				case 5:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "DietaryFlag" : "DietaryFlag DESC";
					break;

				case 6:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "NumOfViews" : "NumOfViews DESC";
					break;

				case 7:
					validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Description" : "Description DESC";
					break;
			}

			// Map Search > searchable columns
			if (!string.IsNullOrEmpty(dtQueryParams.Search.Value))
			{
				//limit to fields in view model
				validQueryParams.Search = dtQueryParams.Search.Value;
			}

			if (string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetProductDto>();
			}

			// query based on filter
			var result = await _productRepository.GetPagedProductResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetProductDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			// shape data
			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedDataTableResponse<IEnumerable<Entity>>(shapeData, dtQueryParams.Draw, recordCount);
		}
	}
}