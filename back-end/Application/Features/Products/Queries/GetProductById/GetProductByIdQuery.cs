﻿using AutoMapper;
using Core.Application.Exceptions;
using Core.Application.Features.Products.Queries.GetProducts;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Products.Queries.GetProductById
{
	public class GetProductByIdQuery : IRequest<Response<GetProductDto>>
	{
		public Guid Id { get; set; }

		public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, Response<GetProductDto>>
		{
			private readonly IProductRepositoryAsync _productRepository;
			private readonly IMapper _mapper;

			public GetProductByIdQueryHandler(IProductRepositoryAsync productRepository, IMapper mapper)
			{
				_productRepository = productRepository;
				_mapper = mapper;
			}

			public async Task<Response<GetProductDto>> Handle(GetProductByIdQuery queryParams, CancellationToken cancellationToken)
			{
				var product = await _productRepository.GetByIdAsync(queryParams.Id);
				if (product == null) throw new ApiException($"Product Not Found.");
				return new Response<GetProductDto>(_mapper.Map<GetProductDto>(product));
			}
		}
	}
}