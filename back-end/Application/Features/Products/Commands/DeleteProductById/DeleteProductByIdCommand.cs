﻿using Core.Application.Exceptions;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Products.Commands.DeleteProductById
{
	public class DeleteProductByIdCommand : IRequest<Response<Guid>>
	{
		public Guid Id { get; set; }

		public class DeleteProductByIdCommandHandler : IRequestHandler<DeleteProductByIdCommand, Response<Guid>>
		{
			private readonly IProductRepositoryAsync _productRepository;
			private readonly IFileService _fileService;
			private readonly IConfiguration _config;

			public DeleteProductByIdCommandHandler(
				IProductRepositoryAsync productRepository,
				IFileService fileService,
				IConfiguration config)
			{
				_productRepository = productRepository;
				_fileService = fileService;
				_config = config;
			}

			public async Task<Response<Guid>> Handle(DeleteProductByIdCommand command, CancellationToken cancellationToken)
			{
				var product = await _productRepository.GetByIdAsync(command.Id);
				if (product == null) throw new ApiException($"Product Not Found.");

				var relativeRemovePath = string.Format(
					"{0}{1}{2}",
					_config.GetSection("UploadPaths:Products").Value,
					command.Id,
					"/"
				);

				_fileService.Remove(relativeRemovePath);

				await _productRepository.DeleteAsync(product);
				return new Response<Guid>(product.Id, "SucceededDelete");
			}
		}
	}
}