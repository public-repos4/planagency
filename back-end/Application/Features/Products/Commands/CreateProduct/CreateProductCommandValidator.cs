﻿using FluentValidation;
using System.Linq;

namespace Core.Application.Features.Products.Commands.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.Description)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.Price)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(x => x.Image)
                .NotNull().WithMessage("{PropertyName} is required");

            RuleFor(x => x.Image.FileName)
                .Must(HaveSupportedFileType).WithMessage("png, gif, jpeg, jpg only are allowed")
                .When(x => x.Image != null);

            RuleFor(p => p.NumOfViews)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.DietaryFlagId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();
        }

        private bool HaveSupportedFileType(string fileName)
        {
            string[] allowedFileTypes = { ".png", ".gif", ".jpeg", ".jpg", ".txt", ".pdf" };
            return allowedFileTypes.Contains(System.IO.Path.GetExtension(fileName));
        }
    }
}