﻿using AutoMapper;
using Core.Application.Enums;
using Core.Application.Exceptions;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Products.Commands.UpdateProduct
{
	public class UpdateProductCommand : IRequest<Response<Guid>>
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public float Price { get; set; }
		public IFormFile Image { get; set; }
		public long NumOfViews { get; set; }
		public DietaryFlags DietaryFlagId { get; set; }
		public Guid CategoryId { get; set; }
	}
	public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Response<Guid>>
	{
		private readonly IProductRepositoryAsync _productRepository;
		private readonly IMapper _mapper;
		private readonly IFileService _fileService;
		private readonly IConfiguration _config;

		public UpdateProductCommandHandler(
			IProductRepositoryAsync productRepository,
			IMapper mapper,
			IFileService fileService,
			IConfiguration config)
		{
			_productRepository = productRepository;
			_mapper = mapper;
			_fileService = fileService;
			_config = config;
		}

		public async Task<Response<Guid>> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
		{
			var product = await _productRepository.GetByIdAsync(command.Id);

			if (product == null)
			{
				throw new ApiException($"Product Not Found.");
			}
			else
			{
				var relativeRemovePath = string.Format(
					"{0}{1}{2}",
					_config.GetSection("UploadPaths:Products").Value,
					product.Id,
					"/"
				);

				_fileService.Remove(relativeRemovePath);

				var relativeUploadPath = relativeRemovePath;

				string imgFullFilePath = _fileService.Upload(command.Image, relativeUploadPath);

				product = _mapper.Map<Product>(command);
				product.Image = imgFullFilePath;

				await _productRepository.UpdateAsync(product);

				return new Response<Guid>(product.Id, "SucceededUpdate");
			}
		}
	}
}