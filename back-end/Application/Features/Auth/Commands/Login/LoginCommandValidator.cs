﻿using Core.Application.Features.Auth.Commands.Login;
using FluentValidation;

namespace Core.Application.Features.Positions.Commands.Login
{
	public class LoginCommandValidator : AbstractValidator<LoginCommand>
	{
		public LoginCommandValidator()
		{
			RuleFor(p => p.UserName)
				.NotEmpty().WithMessage("{PropertyName} is required.")
				.NotNull();

			RuleFor(p => p.Password)
				.NotEmpty().WithMessage("{PropertyName} is required.")
				.NotNull();
		}
	}
}