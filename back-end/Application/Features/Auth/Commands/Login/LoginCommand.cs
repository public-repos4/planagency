﻿using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Core.Domain.Settings;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Auth.Commands.Login
{
	public partial class LoginCommand : IRequest<Response<object>>
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}

	public class LoginCommandHandler : IRequestHandler<LoginCommand, Response<object>>
	{
		private readonly IConfiguration _config;
		private readonly UserManager<User> _usersManager;
		private readonly SignInManager<User> _signInManager;

		public LoginCommandHandler(IConfiguration config, UserManager<User> usersManager, SignInManager<User> signInManager)
		{
			_usersManager = usersManager;
			_signInManager = signInManager;
			_config = config;
		}

		public async Task<Response<object>> Handle(LoginCommand command, CancellationToken cancellationToken)
		{
			User appUser = await _usersManager.Users.FirstOrDefaultAsync(u =>
				u.UserName == command.UserName.ToLower());

			if (appUser == null)
				throw new ApiException("InvalidLoginAttempt"); // Incorrect credential (details: incorrect Username)


			var isCorrect = await _usersManager.CheckPasswordAsync(appUser, command.Password);

			if (!isCorrect)
				throw new ApiException("InvalidLoginAttempt"); // Incorrect credential (details: incorrect Username)

			//var signingInResult = await _signInManager.PasswordSignInAsync(command.UserName, command.Password, false, false);

			//if (!signingInResult.Succeeded)
			//	throw new ApiException("InvalidLoginAttempt"); // Incorrect credential (details: incorrect Username)

			return new Response<object>(
				new
				{
					Id = appUser.Id,
					FirstName = appUser.FirstName,
					LastName = appUser.LastName,
					UserName = appUser.UserName,
					Token = GenerateJwtToken(appUser)
				});
		}

		private string GenerateJwtToken(User user)
		{
			List<Claim> claims = new()
			{
				new Claim(type: "UserId", value: user.Id.ToString()),  //For setting CreatedBy, UpdatedBy and DeletedBy (post, put, patch and delete) 
				new Claim(type: "UserName", value: user.UserName)
			};

			var roles = _usersManager.GetRolesAsync(user).Result;

			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimTypes.Role, role));
			}

			IEnumerable<Claim> customClaims = _usersManager.GetClaimsAsync(user).Result;

			foreach (var customClaim in customClaims)
			{
				claims.Add(new Claim(customClaim.Type, customClaim.Value));
			}

			SymmetricSecurityKey symmetricSecurityKey = new(Encoding.UTF8.GetBytes(_config.GetSection("JWTSettings:Key").Value));

			SigningCredentials signingCredentials = new(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

			SecurityTokenDescriptor securityTokenDescriptor = new()
			{
				Subject = new ClaimsIdentity(claims),
				Expires = DateTime.Now.AddDays(100),
				SigningCredentials = signingCredentials
			};

			JwtSecurityTokenHandler jwtSecurityTokenHandler = new();

			SecurityToken securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);

			return jwtSecurityTokenHandler.WriteToken(securityToken);
		}
	}
}