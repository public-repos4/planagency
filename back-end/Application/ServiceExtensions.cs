﻿using Core.Application.Behaviours;
using Core.Application.Features.Categories.Queries.GetCategories;
using Core.Application.Features.Products.Queries.GetProducts;
using Core.Application.Helpers;
using Core.Application.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Core.Application
{
	public static class ServiceExtensions
	{
		public static void AddApplicationLayer(this IServiceCollection services)
		{
			services.AddAutoMapper(Assembly.GetExecutingAssembly());
			services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
			services.AddMediatR(Assembly.GetExecutingAssembly());
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));

			services.AddScoped<IDataShapeHelper<GetCategoryDto>, DataShapeHelper<GetCategoryDto>>();
			services.AddScoped<IDataShapeHelper<GetProductDto>, DataShapeHelper<GetProductDto>>();

			services.AddScoped<IModelHelper, ModelHelper>();
			services.AddScoped<IEnumHelper, EnumHelper>();
		}
	}
}