﻿
using System;
using System.Collections.Generic;

namespace Core.Application.Parameters
{
    public class QueryParameter : PagingParameter
    {
        public virtual bool IsForConfig { get; set; }

        public Guid? Id { get; set; }
        public IEnumerable<Guid> Ids { get; set; }
        public string Search { get; set; }

        public virtual string Fields { get; set; }
        public virtual string OrderBy { get; set; }

    }
}