﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Application.Interfaces
{
	public interface IGenericRepositoryAsync<T> where T : class
	{
		Task<T> AddAsync(T entity);

		Task<T> GetByIdAsync(Guid id);
		Task<IEnumerable<T>> GetAllAsync();
		Task<IEnumerable<T>> GetPagedReponseAsync(int pageNumber, int pageSize);
		Task<IEnumerable<T>> GetPagedAdvancedReponseAsync(int pageNumber, int pageSize, string orderBy, string fields);
		IQueryable<T> PaginateData(IQueryable<T> query, int? pageNumber, int? pageSize);

		Task UpdateAsync(T entity);

		Task DeleteAsync(T entity);

		 Task Commit();
	}
}