﻿using Core.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Core.Application.Interfaces
{
	public interface IFileService
	{
		string Upload(IFormFile File, string relativeUploadPath);
		string Upload(IFormFile File, string webRootPath, string relativeUploadPath);
		void Upload(IFormFileCollection Files, string uploadPath, long attachedTo);
		void Remove(string relativeRemovePath);
		//void Remove(long id);
	}
}