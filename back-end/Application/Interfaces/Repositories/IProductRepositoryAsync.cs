﻿using Core.Application.Features.Products.Queries.GetProducts;
using Core.Application.Parameters;
using Core.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Interfaces.Repositories
{
	public interface IProductRepositoryAsync : IGenericRepositoryAsync<Product>
	{
		Task<(IEnumerable<Product> data, RecordsCount recordsCount)> GetPagedProductResponseAsync(GetProductsQuery queryParams);
	}
}