﻿using Core.Application.Features.Categories.Queries.GetCategories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Interfaces.Repositories
{
    public interface ICategoryRepositoryAsync : IGenericRepositoryAsync<Category>
    {
        Task<(IEnumerable<Category> data, RecordsCount recordsCount)> GetPagedCategoryResponseAsync(GetCategoriesQuery queryParams);
    }
}