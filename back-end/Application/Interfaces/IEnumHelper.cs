﻿using Core.Application.Enums;
using Core.Application.Features.Categories.Queries.GetCategories;
using System.Collections.Generic;

namespace Core.Application.Interfaces
{
	public interface IEnumHelper
	{
		//public static string GetEnumDescription<T>();
		IEnumerable<GetEnumDto> GetEnumAsList<T>();
		GetEnumDto GetEnumValueAsObject<TEnum>(int value);
	}
}