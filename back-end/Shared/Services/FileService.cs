﻿using Core.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Reflection;

namespace Infrastructure.Shared.Services
{
	public class FileService : IFileService
	{
		public string Upload(IFormFile File, string relativeUploadPath)
		{
			string baseWebApiUrl = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") switch
			{
				// Local host
				"Development" => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/bin" }, StringSplitOptions.None)[0],

				// Online host
				_ => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/Shared.dll" }, StringSplitOptions.None)[0],
			};


			string fullUploadFolderPath = Path.Combine(
			baseWebApiUrl +
			"/wwwroot" +
			relativeUploadPath);

			fullUploadFolderPath = fullUploadFolderPath.Replace("/", @"\");
			fullUploadFolderPath = fullUploadFolderPath.Replace("%20", " ");

			// Upload to this folder after creating it (if not exists)
			Directory.CreateDirectory(fullUploadFolderPath);

			string newFileName = Guid.NewGuid().ToString();

			string fullFilePath = Path.Combine(
				fullUploadFolderPath +
				newFileName +
				Path.GetExtension(File.FileName));

			fullFilePath = fullFilePath.Replace("/", @"\");
			fullFilePath = fullFilePath.Replace("%20", " ");

			using (var fileStreams = new FileStream(fullFilePath, FileMode.Create))
			{
				File.CopyTo(fileStreams);
			}

			return Path.Combine(relativeUploadPath + newFileName + Path.GetExtension(File.FileName));

		}

		public string Upload(IFormFile File, string webRootPath, string relativeUploadPath)
		{
			string baseWebApiUrl = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") switch
			{
				// Local host
				"Development" => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/bin" }, StringSplitOptions.None)[0],

				// Online host
				_ => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/Shared.dll" }, StringSplitOptions.None)[0],
			};

			string fullUploadFolderPath = Path.Combine(webRootPath + relativeUploadPath);

			fullUploadFolderPath = fullUploadFolderPath.Replace("/", @"\");
			fullUploadFolderPath = fullUploadFolderPath.Replace("%20", " ");

			// Upload to this folder after creating it (if not exists)
			Directory.CreateDirectory(fullUploadFolderPath);

			string newFileName = Guid.NewGuid().ToString();

			string fullFilePath = Path.Combine(
				fullUploadFolderPath +
				newFileName +
				Path.GetExtension(File.FileName));

			fullFilePath = fullFilePath.Replace("/", @"\");
			fullFilePath = fullFilePath.Replace("%20", " ");

			using (var fileStreams = new FileStream(fullFilePath, FileMode.Create))
			{
				File.CopyTo(fileStreams);
			}

			return Path.Combine(webRootPath + relativeUploadPath + newFileName + Path.GetExtension(File.FileName));
		}

		public void Upload(IFormFileCollection Files, string uploadPath, long attachedTo)
		{
			throw new NotImplementedException();
		}

		public void Remove(string relativeRemovePath)
		{
			// fullRemovePath (remove folder or file)
			string fullRemovePath = Path.Combine(
				new Uri(Assembly.GetExecutingAssembly().CodeBase)
				.AbsolutePath.Split(new string[] { "/bin" }, StringSplitOptions.None)[0] +
				"/wwwroot" +
				relativeRemovePath);

			fullRemovePath = fullRemovePath.Replace("/", @"\");
			fullRemovePath = fullRemovePath.Replace("%20", " ");

			if (System.IO.File.Exists(fullRemovePath))
			{
				System.IO.File.Delete(fullRemovePath);

			}
			else if (Directory.Exists(fullRemovePath))
			{
				Directory.Delete(fullRemovePath, true);
			}
		}
	}
}